// SDK常量

var GhostIMUniSDKDefines = {}

// 事件类型
GhostIMUniSDKDefines.EVENT = {}
GhostIMUniSDKDefines.EVENT.NET_CHANGED = 'net_changed' // 网络状态变化
GhostIMUniSDKDefines.EVENT.CONVERSATION_LIST_CHANGED = 'conversation_list_changed' // 会话列表变化
GhostIMUniSDKDefines.EVENT.MESSAGE_RECEIVED = 'message_received' // 收到消息
GhostIMUniSDKDefines.EVENT.MESSAGE_REVOKED = 'message_revoked' // 撤回消息
GhostIMUniSDKDefines.EVENT.PRIVATE_READ_RECEIPT = 'private_read_receipt' // 私聊会话已读回执
GhostIMUniSDKDefines.EVENT.FRIEND_LIST_CHANGED = 'friend_list_changed' // 好友列表变化
GhostIMUniSDKDefines.EVENT.FRIEND_APPLY_LIST_CHANGED = 'friend_apply_list_changed' // 好友申请列表变化
GhostIMUniSDKDefines.EVENT.FRIEND_APPLY_REFUSE = 'friend_apply_refuse' // 好友申请被拒
GhostIMUniSDKDefines.EVENT.KICKED_OUT = 'kicked_out' // 用户被踢下线

// 会话类型
GhostIMUniSDKDefines.CONVERSATION_TYPE = {}
GhostIMUniSDKDefines.CONVERSATION_TYPE.PRIVATE = 'private' // 私聊
GhostIMUniSDKDefines.CONVERSATION_TYPE.GROUP = 'group' // 群聊

// 消息类型
GhostIMUniSDKDefines.MESSAGE_TYPE = {}
GhostIMUniSDKDefines.MESSAGE_TYPE.TEXT = 'text' // 文本消息
GhostIMUniSDKDefines.MESSAGE_TYPE.TEXT_AT = 'text_at' // 文本@消息
GhostIMUniSDKDefines.MESSAGE_TYPE.IMAGE = 'image' // 图片消息
GhostIMUniSDKDefines.MESSAGE_TYPE.AUDIO = 'audio' // 语音消息
GhostIMUniSDKDefines.MESSAGE_TYPE.VIDEO = 'video' // 小视频消息
GhostIMUniSDKDefines.MESSAGE_TYPE.LOCATION = 'location' // 位置消息
GhostIMUniSDKDefines.MESSAGE_TYPE.CUSTOM = 'custom' // 自定义消息
GhostIMUniSDKDefines.MESSAGE_TYPE.MERGER = 'merger' // 合并消息
GhostIMUniSDKDefines.MESSAGE_TYPE.FORWARD = 'forward' // 转发消息
GhostIMUniSDKDefines.MESSAGE_TYPE.SYS_NOTICE = 'sys_notice' // 私聊系统通知
GhostIMUniSDKDefines.MESSAGE_TYPE.GROUP_SYS_NOTICE = 'group_sys_notice' // 群聊系统通知

// 用户
GhostIMUniSDKDefines.USER = {}

// 加好友验证方式
GhostIMUniSDKDefines.USER.ADDFRIEND = {}
GhostIMUniSDKDefines.USER.ADDFRIEND.ALLOW = 1 // 允许任何人添加自己为好友
GhostIMUniSDKDefines.USER.ADDFRIEND.CONFIRM = 2 // 需要经过自己确认才能添加自己为好友
GhostIMUniSDKDefines.USER.ADDFRIEND.DENY = 3 // 拒绝加好友

// 群组
GhostIMUniSDKDefines.GROUP = {}

// 群申请处理方式
GhostIMUniSDKDefines.GROUP.JOINMODE = {}
GhostIMUniSDKDefines.GROUP.JOINMODE.FREE = 1 // 自有加入，不需要申请和审核，不需要被邀请人允许。
GhostIMUniSDKDefines.GROUP.JOINMODE.CHECK = 2 // 验证加入，需要申请，以及群主或管理员的同意才能入群
GhostIMUniSDKDefines.GROUP.JOINMODE.FORBIDDEN = 3 // 禁止加入

// 入群申请处理结果
GhostIMUniSDKDefines.GROUP.APPLYSTATUS = {}
GhostIMUniSDKDefines.GROUP.APPLYSTATUS.PENDING = 1 // 待处理
GhostIMUniSDKDefines.GROUP.APPLYSTATUS.AGREE = 2 // 同意
GhostIMUniSDKDefines.GROUP.APPLYSTATUS.REFUSE = 3 // 拒绝

export {
	GhostIMUniSDKDefines
}
