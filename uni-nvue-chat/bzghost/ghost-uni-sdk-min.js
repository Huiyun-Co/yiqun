import WebSocketUtil from './websocket'
import {
	successHandle,
	errHandle
} from './callback'
import {
	GhostIMUniSDKStatusCode
} from './ghost-status-code'

import {
	GhostIMUniSDKDefines
} from './ghost-defines'

import {
	emit,
	addEventListener,
	removeEventListener
} from './event'



import packetCode from './webPacketCode'

import mitt from './mitt.umd'

import { login } from './service/loginService'
import { 
	getGroupList,
	getGroup,
	joinGroupUser
} from './service/groupService'

import { getUserInfo } from './service/userService'

import { getFriendList } from './service/friendService'
import { 
	createTextMessage,
	sendMessage,
	saveMessage,
	getHistoryMessageList
} from './service/messageService'
import { 
	saveCloudConversationListToLocal,
	saveAndUpdateConversation,
	getConversationList
} from './service/conversationService'


import { uploadImage } from './service/uploadService'

// 实例对象
let instance

class GhostIMUniSDK {
    constructor (options = {}) {
        /**
		 * 全局配置参数
		 */
		this.defaults = {

			/**
			 * http url
			 */
			baseURL: undefined,

			/**
			 * socket url
			 */
			socketURL: undefined,

			/**
			 * 事件前缀
			 */
			eventPrefix: 'GhostIM_',

			/**
			 * 最大重连次数，0不限制一直重连
			 */
			reConnectTotal: 15,

			/**
			 * 重连时间间隔
			 */
			reConnectInterval: 3000,

			/**
			 * 心跳时间间隔(默认30s)
			 */
			heartInterval: 30000,

			/**
			 * 	日志等级
			 *  0 普通日志，日志量较多，接入时建议使用
			 *	1 关键性日志，日志量较少，生产环境时建议使用
			 *	2 无日志级别，SDK 将不打印任何日志
			 */
			logLevel: 0,

			/**
			 * SDK自定义初始化参数
			 */
			...options,

			/**
			 * APP离线通知配置
			 */
			notification: {

				/**
				 *  登录后是否自动检测通知权限
				 */
				autoPermission: false,

				/**
				 *  手机安卓8.0以上的通知渠道ID（需要申请）
				 */
				oppoChannelId: 'Default', // OPPO手机安卓8.0以上的通知渠道ID

				/**
				 * 小米手机重要级别消息通知渠道ID（需要申请，一般默认为high_system）
				 */
				xiaomiChannelId: 'high_system',

				...options.notification
			}

		}

		if (typeof uni !== 'undefined') {
			this.uni = true
		} else {
			this.uni = false
		}
        /**
		 * 事件总线
		 */
		this.emitter = mitt()

        /**
		 * 是否首次连接
		 */
		this.firstConnect = true
		/**
		 * webSocket
		 */
		this.webSocket = undefined
		/**
		 * 是否已登陆IMServer
		 */
		this.socketLogged = false
		/**
		 * 是否允许重连
		 */
		this.allowReconnect = true
		/**
		 * 重连锁，防止多重调用
		 */
		this.lockReconnect = false
		/**
		 * 重连次数
		 */
		this.reConnectNum = 0
		/**
		 * 登录连接超时定时器
		 */
		this.connectTimer = undefined
		/**
		 * 心跳定时器
		 */
		this.heartTimer = undefined
		/**
		 * 检测定时器
		 */
		this.checkTimer = undefined
		/**
		 * 用户信息
		 */
		this.user = {}
		/**
		 * 用户ID
		 */
		this.userId = undefined
		/**
		 * 登陆token
		 */
		this.token = undefined
		/**
		 * 媒体上传参数
		 */
		this.mediaUploadParams = {
			"storage": "local",
			"customDomain": null
		}
		/**
		 * 移动端推送标识符
		 */
		this.mobileDeviceId = undefined
		/**
		 * APP是否在前台
		 */
		this.inApp = true
    }

    /**
     * 初始方法
     * @param {*} options
     * @returns
     */
    static init (options = {}) {
        // eslint-disable-next-line no-undef
        if (typeof uni !== 'undefined' && uni.$GhostIMInstance) {
			// eslint-disable-next-line no-undef
			instance = uni.$GhostIMInstance
        }
        if (instance) {
			return instance
        }
        if (!options.baseURL || !options.socketURL) {
			return console.error(GhostIMUniSDKStatusCode.SDK_PARAMS_ERROR.describe)
        }
        instance = new GhostIMUniSDK(options)
        if (typeof uni !== 'undefined') {
			// eslint-disable-next-line no-undef
			uni.$GhostIMInstance = instance
        }
		
		GhostIMUniSDK.prototype.login = login;
		GhostIMUniSDK.prototype.getUserInfo = getUserInfo;
		GhostIMUniSDK.prototype.getGroupList = getGroupList;
		GhostIMUniSDK.prototype.getGroup = getGroup;
		GhostIMUniSDK.prototype.getFriendList = getFriendList;
		GhostIMUniSDK.prototype.saveCloudConversationListToLocal = saveCloudConversationListToLocal;
		GhostIMUniSDK.prototype.getConversationList = getConversationList;
		GhostIMUniSDK.prototype.saveAndUpdateConversation = saveAndUpdateConversation;
		GhostIMUniSDK.prototype.addEventListener = addEventListener;
		GhostIMUniSDK.prototype.removeEventListener = removeEventListener;
		GhostIMUniSDK.prototype.createTextMessage = createTextMessage;
		GhostIMUniSDK.prototype.sendMessage = sendMessage;
		GhostIMUniSDK.prototype.uploadImage = uploadImage;
		GhostIMUniSDK.prototype.joinGroupUser = joinGroupUser;
		GhostIMUniSDK.prototype.getHistoryMessageList = getHistoryMessageList;
		return instance
    }

    /**
     * 获取SDK实例对象
     * @returns
     */
   static getInstance () {
	// eslint-disable-next-line no-undef
		if (typeof uni !== 'undefined' && uni.$GhostIMInstance) {
			// eslint-disable-next-line no-undef
			instance = uni.$GhostIMInstance
		}
		if (instance) {
			return instance
		}
		console.error('SDK未初始化，无法获取实例化对象')
		return undefined
	}

    /**
     *  连接服务器
     * @param {*} options
     * @returns
     */
    connect (options = {}) {
		if (!options.userId || !options.token) {
			return errHandle(options, GhostIMUniSDKStatusCode.NO_USERID.code, GhostIMUniSDKStatusCode.NO_USERID
				.describe)
		}
		// 拼接websocket连接url
		const url = this.defaults.socketURL

		// 创建websocket连接
		this.webSocket = new WebSocketUtil(url)

		// 设置websocket相关监听
		this.webSocket.onOpen(this.socketOpen.bind(this))
		this.webSocket.onClose(this.socketClose.bind(this))
		this.webSocket.onError(this.socketError.bind(this))
		this.webSocket.onMessage(this.socketMessage.bind(this))

		// 连接异常
		if (this.connectTimer) {
			clearTimeout(this.connectTimer)
			this.connectTimer = undefined
		}
		this.connectTimer = setTimeout(() => {
			this.connectTimer = undefined
			return errHandle(options, GhostIMUniSDKStatusCode.CONNECT_ERROR.code, GhostIMUniSDKStatusCode
				.CONNECT_ERROR
				.describe)
		}, this.defaults.reConnectInterval + 1000)

		// 临时设置监听onMessage
		this.webSocket.onMessage((res) => {
			if (this.socketLogged) {
				clearTimeout(this.connectTimer)
				this.connectTimer = undefined
				return
			}
			clearTimeout(this.connectTimer)
			this.connectTimer = undefined
			console.log('====================')
			const data = packetCode.decode(res.data)
			console.log(data)
			// 监听到登陆成功
			if (data.version) {
				console.log(1111111111)
				
				// 设置用户相关参数
				// let user = data.data.user;
				// this.user = user;
				//this.userId = options.userId
				//this.token = options.token

				// 设置socket state（socketLogged）登陆成功
				this.socketLogged = true
				this.reConnectNum = 0

				// 登陆成功后从服务端获取一下全部会话，并发送事件CONVERSATION_LIST_CHANGED
				saveCloudConversationListToLocal();
				// 登陆成功触发一次好友列表更新事件
				// handleFriendListChanged();
				// 登陆成功触发一次好友申请列表更新事件
				// handleFriendApplyListChanged();
				// 获取媒体上传参数
				// getMediaUploadParams();
				// 如果在uni环境下，执行以下操作
				return successHandle(options, GhostIMUniSDKStatusCode.NORMAL_SUCCESS.describe, null)
			} else {
				return errHandle(options, GhostIMUniSDKStatusCode.LOGIN_ERROR.code, GhostIMUniSDKStatusCode
					.LOGIN_ERROR
					.describe)
			}
		})
	}

    /**
	 * 断开连接
	 * 手动操作断开连接后，不会重连
	 */
	disConnect () {
		this.allowReconnect = false
		this.webSocket.close()
		this.webSocket.destroy()
	}

    /**
	 * 重连
	 */
	reConnect () {
		if (this.allowReconnect && !this.lockReconnect) {
			// 没有用户信息，取消重连
			if (!this.userId || !this.token) {
				this.allowReconnect = false
				return
			}
			// 设置了最大重连次数，并且已经到了限制，不再连。
			if (this.defaults.reConnectTotal > 0 && this.defaults.reConnectTotal <= this.reConnectNum) {
				this.allowReconnect = false
				return
			}
			this.reConnectNum++
			this.lockReconnect = true
			// 没连接上会一直重连，设置延迟避免请求过多
			// 通知网络状态变化
			emit(GhostIMUniSDKDefines.EVENT.NET_CHANGED, 'connecting')
			setTimeout(() => {
				try {
					this.connect({
						userId: this.userId,
						token: this.token
					})
				} catch (e) {
					console.log(e)
				}
				this.lockReconnect = false
			}, this.defaults.reConnectInterval) // 这里设置重连间隔(ms)
		}
	}

    /**
	 * 获取当前连接状态
	 * "CLOSED":3,"CLOSING":2,"CONNECTING":0,"OPEN":1
	 */
	readyState () {
		if (this.webSocket && this.webSocket.socketTask) {
			return this.webSocket.socketTask.readyState
		} else {
			return 3
		}
	}

	/**
	 * 开始心跳
	 */
	startHeartTimer () {
		this.clearHeartTimer()
		if (this.webSocket && this.webSocket.socketTask) {
			this.heartTimer = setTimeout(() => {
				if (this.readyState() === 1) {
					const packet = {
						version: 1,
						command: 17
					}
					this.webSocket.send(packet)
					this.checkTimer = setTimeout(() => {
						this.webSocket.close()
					}, this.defaults.heartInterval)
				}
			}, this.defaults.heartInterval)
		}
	}
	/**
	 * 发送消息
	 */
	send (params) {
		params.version = 1
		params.command = 3
		this.webSocket.send(params)
	}

	/**
	 * 加入用户所在群组通道
	 */
	joinUserGroup (params) {
		params.version = 1
		params.command = 1
		this.webSocket.send(params)
	}
	/**
	 * 加入群组
	 */
	joinGroup (params) {
		params.version = 1
		params.command = 7
		this.webSocket.send(params)
	}
	/**
	 * 退出群组
	 */
	quitGroup (params) {
		params.version = 1
		params.command = 9
		this.webSocket.send(params)
	}

	/**
	 * 清除心跳定时器，取消心跳
	 */
	clearHeartTimer () {
		clearTimeout(this.heartTimer)
		clearTimeout(this.checkTimer)
		return this
	}

	/**
	 * socket 打开回调
	 * @param {Object} info
	 */
	socketOpen (info) {
		emit(GhostIMUniSDKDefines.EVENT.NET_CHANGED, 'connected')
	}

	/**
	 * socket 关闭回调
	 * @param {Object} _err
	 */
	socketClose (_err) {
		this.socketLogged = false
		this.webSocket.close()
		this.webSocket.destroy()
		emit(GhostIMUniSDKDefines.EVENT.NET_CHANGED, 'closed')
		this.reConnect()
	}

	/**
	 * socket 错误回调
	 * @param {Object} _err
	 */
	socketError (_err) {
		this.reConnect()
	}

	/**
	 * socket 消息回调
	 * @param {Object} res
	 */
	socketMessage (res) {
		// 收到任意消息代表心跳成功
		this.startHeartTimer()
		if (!this.socketLogged) return
		console.log('==============')
		let data = packetCode.decode(res.data);
		if (data) {
			this.socketMessageHandle(data);
		} else {
		}
	}

	/**
	 * WebSocket消息统一处理
	 * @param {Object} data
	 */
	socketMessageHandle (data) {
		if (data.code === 200) {
			// 收到消息
			const message = data

			// 1. 保存消息,会话数据由在线推送
			// eslint-disable-next-line no-undef
			saveMessage(message)

			// 2. 发送消息接收事件
			emit(GhostIMUniSDKDefines.EVENT.MESSAGE_RECEIVED, message)
		} else if (data.code === 203) {
			// 收到会话更新
			const conversation = data
			// eslint-disable-next-line no-undef
			saveAndUpdateConversation(conversation)
		} else if (data.code === 205) {
			// 收到私聊会话已读回执
			const conversation = data
			// eslint-disable-next-line no-undef
			handlePrivateConversationReadReceipt(conversation.conversationId)
		} else if (data.code === 207) {
			// 收到消息撤回事件
			// handleMessageRevoked(data)
		} else if (data.code === 208) {
			// 好友列表更新事件
			setTimeout(() => {
				// handleFriendListChanged()
			}, 500)
		} else if (data.code === 209) {
			// 好友申请列表更新
			setTimeout(() => {
				// handleFriendApplyListChanged()
			}, 500)
		} else if (data.code === 210) {
			// 好友申请被拒绝
			emit(GhostIMUniSDKDefines.EVENT.FRIEND_APPLY_REFUSE, data)
			// 更新好友申请列表
			setTimeout(() => {
				// handleFriendApplyListChanged(1)
			}, 500)
		} else if (data.code === 10009) {
			// 被踢下线不允许重连
			this.allowReconnect = false
			this.clearHeartTimer()
			this.webSocket.close()
			emit(GhostIMUniSDKDefines.EVENT.KICKED_OUT, true)
		}
	}

	/**
	 * 拦截登陆状态
	 */
	checkLogged () {
		if (!this.socketLogged || !this.userId || !this.token) {
			return false
		} else {
			return true
		}
	}

	/**
	 * 设置APP在前台
	 */
	intoApp () {
		this.inApp = true
	}

	/**
	 * 设置APP已进入后台
	 */
	leaveApp () {
		this.inApp = false
	}
}

export {
    instance,
	GhostIMUniSDK,
    GhostIMUniSDKDefines
}
