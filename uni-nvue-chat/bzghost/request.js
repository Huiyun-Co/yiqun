/*eslint-disable*/
import {
	GhostIMUniSDKStatusCode
} from './ghost-status-code';
import {
	fetch
} from './fetch';
import queryParams from './queryParams';
import {
	instance
} from './ghost-uni-sdk-min';
import {
	errHandle,
	successHandle
} from './callback';


/**
 * 公共请求 
 * 
 *
 * @param {String} url - 请求路由
 * @param {String} method - 请求方法
 * @param {Object} data - 请求参数
 * 
 * @return uni.request Promise
 *  
 */
function request(url, method = 'GET', data = null) {

	return new Promise((resolve, reject) => {

		if (instance.uni) {
			uni.request({
				url: instance.defaults.baseURL + url,
				data: data,
				method: method,
				header: {
					'content-type': 'application/json',
					'token': instance.token != null ? instance.token : ''
				},
				success: (result) => {
					if (result.data == null) {
						return reject({
							code: GhostIMUniSDKStatusCode.NORMAL_ERROR.code,
							message: result.message ? result.message : YeIMUniSDKStatusCode
								.NORMAL_ERROR.describe,
							data: null
						});
					}
					result = result.data;
					let code = result.state;
					console.log('-------11111111----' + code)
					if (code === GhostIMUniSDKStatusCode.NORMAL_SUCCESS.code) {
						resolve(result.data ? result.data : null);
					} else {
						reject(result);
					}
				},
				fail: (fail) => {
					reject({
						code: GhostIMUniSDKStatusCode.NORMAL_ERROR.code,
						message: JSON.stringify(fail),
						data: null
					});
				}
			});
		} else {

			let options = {
				method: method,
				body: JSON.stringify(data),
				headers: {
					"Content-Type": "application/json",
					'token': instance.token != null ? instance.token : ''
				},
			};

			if (method == "GET") {
				url = url + queryParams(data, true);
				delete options.body;
			}

			fetch(instance.defaults.baseURL + url, options).then(async (response) => {
				let result = await response.json();
				if (result == null) {
					return reject({
						code: GhostIMUniSDKStatusCode.NORMAL_ERROR.code,
						message: result.message ? result.message : GhostIMUniSDKStatusCode
							.NORMAL_ERROR.describe,
						data: null
					});
				}
				let code = 200;
				console.log('-------22222222----')
				if (code === GhostIMUniSDKStatusCode.NORMAL_SUCCESS.code) {
					resolve(result.result ? result.result : null);
				} else {
					reject(result);
				}
			}, (error) => {
				reject({
					code: GhostIMUniSDKStatusCode.NORMAL_ERROR.code,
					message: JSON.stringify(error.message),
					data: null
				});
			})
		}
	});
}

/**
 * 公共上传
 * 
 * @param {Object} options - 请求参数
 * @param {String} options.url - 请求地址
 * @param {String} options.name - 文件上传的标识符
 * @param {Object} options.header - 请求头
 * @param {Object} options.data - 请求参数
 * @param {String} options.filePath - 上传文件资源的路径
 * @param {Boolean} options.ignoreResult - 是否忽略结果
 * @param {(result)=>{}} [options.success] - 成功回调
 * @param {(error)=>{}} [options.fail] - 失败回调 
 * 
 * @return uploadTask
 *  
 */
function upload(options) {
	return uni.uploadFile({
		url: options.url,
		name: options.name,
		formData: options.data,
		header: options.header,
		filePath: options.filePath,
		success: (result) => {
			//忽略结果
			if (options.ignoreResult) {
				successHandle(options, GhostIMUniSDKStatusCode.NORMAL_SUCCESS.describe)
			} else {
				if (result.data == null) reject(null);
				result = JSON.parse(result.data);
				let code = result.code;
				if (code === GhostIMUniSDKStatusCode.NORMAL_SUCCESS.code) {
					console.log(111)
					console.log(result.data)
					successHandle(options, GhostIMUniSDKStatusCode.NORMAL_SUCCESS.describe, result.data ?
						result.data : null)
				} else {
					errHandle(options, code, result.message);
				}
			}
		},
		fail: (fail) => {
			errHandle(options, GhostIMUniSDKStatusCode.NORMAL_ERROR.code, fail);
		}
	});
}

/**
 * 公共上传
 * 
 * @param {Object} options - 请求参数
 * @param {String} options.url - 请求的下载地址 
 * @param {Object} options.header - 请求头 
 * @param {(result)=>{}} [options.success] - 成功回调
 * @param {(error)=>{}} [options.fail] - 失败回调 
 * 
 * @return downloadTask
 *  
 */
function download(options) {
	return uni.downloadFile({
		url: options.url,
		header: options.header,
		success: (downloadRes) => {
			if (downloadRes.statusCode === 200) {
				successHandle(options, GhostIMUniSDKStatusCode
					.NORMAL_SUCCESS.describe, downloadRes.tempFilePath)
			} else {
				errHandle(options, GhostIMUniSDKStatusCode.DOWNLOAD_ERROR
					.code, GhostIMUniSDKStatusCode.DOWNLOAD_ERROR
					.code.describe);
			}
		},
		fail: (fail) => {
			errHandle(options, GhostIMUniSDKStatusCode.DOWNLOAD_ERROR
				.code, GhostIMUniSDKStatusCode.DOWNLOAD_ERROR
				.code.describe);
		}
	});
}

export {
	request,
	upload,
	download
}