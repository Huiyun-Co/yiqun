import {
	request
} from '../request.js'

import {
	successHandle,
	errHandle
} from '../callback'

import {
	GhostIMUniSDKStatusCode
} from '../ghost-status-code'

import {
	GhostIMUniSDKDefines
} from '../ghost-defines'


function login(options){
	request('/api/rpc/login', 'POST', options).then((result) => {
		successHandle(options, GhostIMUniSDKStatusCode.NORMAL_SUCCESS.describe, result);
	}).catch((fail) => {
		errHandle(options, fail.code, fail.message);
	});
}

export {
	login
}