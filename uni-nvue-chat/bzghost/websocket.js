/*eslint-disable*/
import packetCode from './webPacketCode'
export default class WebSocketUtil {

	socketTask = undefined;

	onMessageCallBack = [];

	constructor(url) {
		if (typeof uni !== 'undefined') {
			this.socketTask = uni.connectSocket({
				url: url,
				success: (e) => { 
					console.log(e)
				},
				fail: (e) => { console.log(e)},
				complete: (e) => { console.log(e)}
			});
		} else {
			this.socketTask = new WebSocket(url);
			this.socketTask.binaryType = "arraybuffer";
			this.socketTask.onmessage = (res) => {
				if (this.onMessageCallBack) {
					for (let index = 0; index < this.onMessageCallBack.length; index++) {
						this.onMessageCallBack[index](res);
					}
				}
			};
		}
	}
	onOpen(callback) {
		try {
			if (typeof uni !== 'undefined') {
				this.socketTask.onOpen(callback);
			} else {
				this.socketTask.onopen = callback;
			}
		} catch (e) {
			console.log(e)
		}
	}
	onClose(callback) {
		try {
			if (typeof uni !== 'undefined') {
				this.socketTask.onClose(callback);
			} else {
				this.socketTask.onclose = callback;
			}
		} catch (e) {
			console.log(e)
		}
	}
	onError(callback) {
		try {
			if (typeof uni !== 'undefined') {
				this.socketTask.onError(callback);
			} else {
				this.socketTask.onerror = callback;
			}
		} catch (e) {
			console.log(e)
		}
	}
	onMessage(callback) {
		try {
			if (typeof uni !== 'undefined') {
				this.socketTask.onMessage(callback);
			} else {
				this.onMessageCallBack.push(callback);
			}
		} catch (e) {
			console.log(e)
		}
	}
	send(str) {
		if (this.socketTask) {
			try {
				if (typeof uni !== 'undefined') {
					this.socketTask.send({
						data: packetCode.encode(str),
						success(res) {
							console.log(res)
						},
						fail(err) {
							console.log(err)
						}
					});
				} else {
					this.socketTask.send(packetCode.encode(str));
				}
			} catch (e) {
				console.log(e)
			}
		}
	}
	close() {
		try {
			this.socketTask.close();
		} catch (e) {
			console.log(e)
		}
	}
	destroy() {
		this.socketTask = undefined;
	}
}
