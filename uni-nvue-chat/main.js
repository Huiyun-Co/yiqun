import Vue from 'vue'
import App from './App'
import store from './store';
Vue.config.productionTip = false
Vue.prototype.$store = store

App.mpType = 'app'
// import vConsole from './common/vconsole.min.js';
// var VConsole = new vConsole();
import { GhostIMUniSDK } from "./bzghost/ghost-uni-sdk-min.js";
	//实例化GhostIMUniSDK
	GhostIMUniSDK.init({
		baseURL: 'http://123.207.67.48:9999',
		socketURL: 'ws://123.207.67.48:9998/chat',
		logLevel: 0, // SDK日志等级，0 = 打印全部日志， 1 = 打印重要日志，2 = 不打印日志
		reConnectInterval: 3000, // 断线重连时间间隔（遇到网络波动，IM服务端可能会断线，此时需要重新连接到服务端），单位：毫秒
		reConnectTotal: 0, // 最大重连次数，0不限制一直重连
		heartInterval: 30000, //心跳时间间隔（默认30秒），单位：毫秒
	});
Vue.prototype.$GhostIMUniSDK = GhostIMUniSDK
const app = new Vue({
	store,
	...App
})
app.$mount()
